#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
# SPDX-License-Identifier: AGPL-3.0-or-later


from setuptools import setup

setup(
    name='oe1journalfeedfilters',
    version="0.0",
    description='minimalistic service for filtering the offical Ö1 Journal feed by braodcast format',
    author='Michael Pöhn',
    author_email='michael@poehn.at',
    url='https://codeberg.org/uniqx/oe1journalfeedfilters',
    license='AGPL-3.0-or-later',
    packages=[
        'oe1journalfeedfilters',
    ],
    python_requires='>=3.4',
    install_requires=[
        'defusedxml==0.7.1',
        'tornado==6.2',
    ],
    package_data = {
        'oe1journalfeedfilters': [
            'static/icon/*',
            'templates/*',
        ],
    },
)
