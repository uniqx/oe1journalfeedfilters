<!--
SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
SPDX-License-Identifier: AGPL-3.0-or-later
-->

# OE1 Journal Feed Filters

Simple microservice for splitting OE1 Journal Podcast feed, into multiple
smaller more specific feeds.

https://codeberg.org/uniqx/oe1journalfeedfilters

## minimal install

```!bash
# install from git using pip
python3 -m pip install git+https://codeberg.org/uniqx/oe1journalfeedfilters.git@main

# show cli options
python3 -m oe1journalfeedfilters --help

# run server
python3 -m oe1journalfeedfilters
```

## License

This project is licensed under AGPL-3.0-or-later and copyright/license
attributions are `reuse` compliant.
