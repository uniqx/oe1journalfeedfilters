#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
# SPDX-License-Identifier: AGPL-3.0-or-later

import oe1journalfeedfilters.main

if __name__ == "__main__":
    oe1journalfeedfilters.main.main()
