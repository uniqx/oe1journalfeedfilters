#! /usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: AGPL-3.0-or-later

import os
import io
import signal
import argparse
import urllib.request
import tornado.web
import tornado.ioloop
import tornado.httpclient
import defusedxml.ElementTree
import xml.etree.ElementTree


def template_path(template):
    return os.path.join(
        os.path.abspath(os.path.dirname(__file__)), "templates", template
    )

class IndexHandler(tornado.web.RequestHandler):
    async def get(self):
        self.render(template_path("index.html"))


async def _fetch_and_filter(filter_text):
    http_client = tornado.httpclient.AsyncHTTPClient()
    response = await http_client.fetch(
        "https://podcast.orf.at/podcast/oe1/oe1_journale/oe1_journale.xml"
    )
    tree = defusedxml.ElementTree.fromstring(response.body)
    removelist = []
    for item in tree.findall("./channel/item"):
        title = item.find("title").text
        if filter_text not in title:
            removelist.append(item)
    channel = tree.find("./channel")
    channel.find("./title").text = "Ö1 " + filter_text

    # make sure podcast clients don't move away from filtered feed new official
    # original podcast url
    new_feed_url = channel.find("./{http://www.itunes.com/dtds/podcast-1.0.dtd}new-feed-url")
    if not new_feed_url is None:
        channel.remove(new_feed_url)

    for item in removelist:
        channel.remove(item)
    return xml.etree.ElementTree.tostring(tree, encoding="utf-8", method="xml")


class FruhjournalHandler(tornado.web.RequestHandler):
    async def get(self):
        raw_resp = await _fetch_and_filter("Frühjournal")
        self.set_header("Content-Type", "text/xml; charset=utf-8;")
        self.write(raw_resp)


class MorgenjournalUm7Handler(tornado.web.RequestHandler):
    async def get(self):
        raw_resp = await _fetch_and_filter("Morgenjournal um 7")
        self.set_header("Content-Type", "text/xml; charset=utf-8;")
        self.write(raw_resp)


class MorgenjournalUm8Handler(tornado.web.RequestHandler):
    async def get(self):
        raw_resp = await _fetch_and_filter("Morgenjournal um 8")
        self.set_header("Content-Type", "text/xml; charset=utf-8;")
        self.write(raw_resp)


class MittagsjournalHandler(tornado.web.RequestHandler):
    async def get(self):
        raw_resp = await _fetch_and_filter("Mittagsjournal")
        self.set_header("Content-Type", "text/xml; charset=utf-8;")
        self.write(raw_resp)


class AbendjournalHandler(tornado.web.RequestHandler):
    async def get(self):
        raw_resp = await _fetch_and_filter("Abendjournal")
        self.set_header("Content-Type", "text/xml; charset=utf-8;")
        self.write(raw_resp)


async def shutdown():
    tornado.ioloop.IOLoop.current().stop()


def exit_handler(sig, frame):
    print("shutting down ...")
    tornado.ioloop.IOLoop.instance().add_callback_from_signal(shutdown)


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "--debug",
        "-d",
        action="store_true",
        default=False,
        help="If this flag is present, the app will start in debug mode. "
        "This is only useful for development, never activate this on your "
        "servers.",
    )
    ap.add_argument(
        "--port",
        "-p",
        type=int,
        default=8371,
        help="set the network port on which this application server is "
        "supposed to listen on (default: 8371)",
    )
    ap.add_argument(
        "--address",
        "-a",
        type=str,
        default="127.0.0.1",
        help="set the network address on which this application server is "
        "supposed to listen on (default: 127.0.0.1)",
    )
    args = ap.parse_args()

    app = tornado.web.Application(
        (
            (r"/", IndexHandler),
            (r"/fruhjournal.rss", FruhjournalHandler),
            (r"/morgenjournalum7.rss", MorgenjournalUm7Handler),
            (r"/morgenjournalum8.rss", MorgenjournalUm8Handler),
            (r"/mittagsjournal.rss", MittagsjournalHandler),
            (r"/abendjournal.rss", AbendjournalHandler),
        ),
        debug=args.debug,
        static_path=os.path.join(os.path.abspath(os.path.dirname(__file__)), "static"),
    )

    # globally register itunse namespaces becuase that's the only way I could
    # find to make etree print this namespace correctly
    xml.etree.ElementTree.register_namespace(
        "itunes", "http://www.itunes.com/dtds/podcast-1.0.dtd"
    )

    app.listen(args.port, args.address)
    signal.signal(signal.SIGTERM, exit_handler)
    signal.signal(signal.SIGINT, exit_handler)
    print("listening for requests on: http://{}:{}".format(args.address, args.port))
    tornado.ioloop.IOLoop.current().start()
